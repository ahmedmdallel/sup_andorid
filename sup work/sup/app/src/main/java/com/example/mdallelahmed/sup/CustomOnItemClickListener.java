package com.example.mdallelahmed.sup;

import android.view.View;

public interface CustomOnItemClickListener {
    public void onItemClick(View v, int i);
}
