package com.example.mdallelahmed.sup;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class ToDoListActivity extends AppCompatActivity {
       private String username,password;
       public  ArrayList<String> list=new ArrayList<>();
       public  ArrayList<String> listid=new ArrayList<>();
       public RecyclerView recyclerView;
       public String id;
       public Intent intent ;

    FloatingActionButton floatingActionButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
        Intent intent2 = getIntent();
        floatingActionButton = findViewById(R.id.floatingActionAdd);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        username =  intent2.getStringExtra("username");
        password =  intent2.getStringExtra("password");
        /*
           add new to do
         */
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToDoListActivity.this, ToDoActivity.class);
                intent.putExtra("todo", "");
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                intent.putExtra("id","");
                 listid=setlistid(username,password);
                 intent.putExtra("listid",listid);
                startActivity(intent);
            }
        });
       readtodolist(username,password);
       /*
       Every thirty seconds the smartphone refresh the displayed todolis
        */
        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(30000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                list.clear();
                               readtodolist(username,password);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        thread.start();

    }
    /*
///function to check internet connection
*/
    public boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        else
            return false;
    }
    /*
    function read to do list from API
     */
    public void readtodolist(final String username, final String password){
               if(checkConnection()) {
                   RequestQueue requestQueuereg = Volley.newRequestQueue(this);
                   StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=list&username=" + username + "&password=" + password, new Response.Listener<String>() {
                       @Override
                       public void onResponse(String response) {
                           JSONArray jsonResponse = null;

                           try {
                               jsonResponse = new JSONArray(response);

                               for (int i =0; i<jsonResponse.length();i++){
                                   String todo="TO DO LIST "+jsonResponse.getJSONObject(i).getString("id")+":\n"+jsonResponse.getJSONObject(i).getString("todo")+"\n"+"LAST UPADTE ON  "+jsonResponse.getJSONObject(i).getString("lastupdate");
                                   list.add(todo);
                                   listid.add(jsonResponse.getJSONObject(i).getString("id"));
                               }
                               intent = new Intent(ToDoListActivity.this, ToDoActivity.class);
                               intent.putExtra("listid",listid);

                               CustomAdapter customAdapter = new CustomAdapter(list, new CustomOnItemClickListener() {
                                   @Override
                                   public void onItemClick(View v, int i) {
                                       TextView textView = (TextView) v;
                                       intent.putExtra("todo", textView.getText().toString());
                                       intent.putExtra("username", username);
                                       intent.putExtra("password", password);
                                       intent.putExtra("id",textView.getText().toString().substring(11,textView.getText().toString().indexOf(":")));

                                       startActivity(intent);
                                   }
                               });
                               recyclerView.setAdapter(customAdapter);
                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                       }
                   }, new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError error) {
                           Log.i("err", error.toString());
                       }
                   });

                   requestQueuereg.add(requestreg);
               }





    }
    /*
    function get all to do id
     */
    public ArrayList<String> setlistid(String username, String password){
        if(checkConnection()) {
            RequestQueue requestQueuereg = Volley.newRequestQueue(this);
            StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=list&username=" + username + "&password=" + password, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONArray jsonResponse = null;

                    try {
                        jsonResponse = new JSONArray(response);
                        for (int i =0; i<jsonResponse.length();i++){
                            listid.add(jsonResponse.getJSONObject(i).getString("id"));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("err", error.toString());
                }
            });

            requestQueuereg.add(requestreg);
            return listid;
        }
        return null;
    }
}
