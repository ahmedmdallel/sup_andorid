package com.example.mdallelahmed.sup;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity {
    SQLiteOpenHelper openHelper;
    SQLiteDatabase db;
    Button btnreg,btnlogin;
    EditText editTextpassword,editTextusername,editTextfname,editTextlname,editTextemail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openHelper = new DatabaseHelper(this);
        btnreg = (Button) findViewById(R.id.submit);
        btnlogin = (Button) findViewById(R.id.buttonlogin);
        editTextusername = (EditText) findViewById(R.id.editTextusername);
        editTextpassword = (EditText) findViewById(R.id.editTextpasswordlogin);
        editTextemail = (EditText) findViewById(R.id.editTextemail);
        editTextfname = (EditText) findViewById(R.id.editTextfname);
        editTextlname = (EditText) findViewById(R.id.editTextlname);
       /*
       function sync SQLite dataBase and mysql data base using Api
        */
        synDb();
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 db = openHelper.getWritableDatabase();
                 String fname= editTextfname.getText().toString();
                String lname= editTextlname.getText().toString();
                String password= editTextpassword.getText().toString();
                String username= editTextusername.getText().toString();
                String email= editTextemail.getText().toString();
                insertdata(fname,lname,password,username,email);
                Toast.makeText(getApplicationContext(),"register successfully",Toast.LENGTH_LONG).show();
                Intent intent =  new Intent(MainActivity.this,LoginActivity2.class);
                startActivity(intent);


            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(MainActivity.this,LoginActivity2.class);
                startActivity(intent);
            }
        });

    }
    /*
    function insert data into SQLite database
     */
    public void insertdata(String fname,String lname,String password,String username,String email){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.COL_2,username);
        contentValues.put(DatabaseHelper.COL_3,password);
        contentValues.put(DatabaseHelper.COL_4,fname);
        contentValues.put(DatabaseHelper.COL_5,lname);
        contentValues.put(DatabaseHelper.COL_6,email);
        long id = db.insert(DatabaseHelper.TABLE_NAME,null,contentValues);
         addUser(username,password,fname,lname,email);

    }
    /*
///function to check internet connection
*/
    public boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        else
            return false;
    }
    public void synDb(){
        db = openHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM users";
        Cursor c = db.rawQuery(selectQuery, null);

        while (c.moveToNext()) {

            final String id,username,password,fristname,lastname,email;
            username = c.getString(c.getColumnIndex("username"));
            password = c.getString(c.getColumnIndex("password"));
            fristname = c.getString(c.getColumnIndex("firstname"));
            lastname = c.getString(c.getColumnIndex("lastname"));
            email = c.getString(c.getColumnIndex("email"));
            final JSONObject jsonflase = new JSONObject();
            try {
                jsonflase.put("success", false);
                jsonflase.put("message", "not found");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(checkConnection()) {
                RequestQueue requestQueuereg = Volley.newRequestQueue(this);
                StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=login&username=" + username + "&password=" + password, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonResponse = null;
                        try {
                            jsonResponse = new JSONObject(response);
                            if(jsonResponse.length()==2){
                                addUser(username,password,fristname,lastname,email);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("err", error.toString());
                    }
                });

                requestQueuereg.add(requestreg);
            }
        }
        c.close();

    }
    /*
    function add user in my sql data base using API
     */
    public void addUser(String username,String password,String fname,String lname,String email){
        if(checkConnection()) {
            RequestQueue requestQueuereg = Volley.newRequestQueue(this);
            StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=register&username=" + username + "&password=" + password + "&firstname=" + fname + "&lastname=" + lname + "&email=" + email, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("succ", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("err", error.toString());

                }
            });

            requestQueuereg.add(requestreg);
        }
    }

}
