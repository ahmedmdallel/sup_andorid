package com.example.mdallelahmed.sup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class ToDoActivity extends AppCompatActivity {
     Button buttonupdate;
     EditText todoedittext;
     String username,password,todo,id;
     FloatingActionButton floatingActionButton;
    public ArrayList<String> listid=new ArrayList<>();
    private String m_Text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);
        buttonupdate = (Button) findViewById(R.id.buttonupdate);
        floatingActionButton = findViewById(R.id.floatingActionButton);
        todoedittext = (EditText) findViewById(R.id.editTexttodo);
        Intent intent2 = getIntent();
        listid =  intent2.getStringArrayListExtra("listid");
        username =  intent2.getStringExtra("username");
        id =  intent2.getStringExtra("id");
        password =  intent2.getStringExtra("password");
        if(!id.equals("")){
            todo = intent2.getStringExtra("todo").substring(intent2.getStringExtra("todo").indexOf(":")+1,intent2.getStringExtra("todo").indexOf("LAST UPADTE ON "));
        }
        todoedittext.setText(todo);

/*
save new information of to do
 */
        buttonupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!id.equals(""))
                udpatetodo(username,id,password,todoedittext.getText().toString());
                else
                {
                    int newid =addlistto();
                    if(newid==0){
                        Toast.makeText(getApplicationContext(),"You can t add new to do list",Toast.LENGTH_LONG).show();
                    }else
                    {
                        save(username,"null",password,todoedittext.getText().toString());
                    }
                }
                Intent intent = new Intent(ToDoActivity.this, ToDoListActivity.class);
                intent.putExtra("username", username);
                intent.putExtra("password", password);
                startActivity(intent);

            }
        });
        /*
        action in floating button
         */
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           alert(username,password);
            }
        });
    }
    /*
///function to check internet connection
*/
    public boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        else
            return false;
    }
    /*
    function update to do
     */
    public void udpatetodo(String username, String id,String password,String todo){
        if(checkConnection()) {
            RequestQueue requestQueuereg = Volley.newRequestQueue(this);
            StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=update&username=" + username +"&id="+id+ "&password=" + password+"&todo="+todo, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject jsonResponse = null;
                    try {
                        jsonResponse = new JSONObject(response);
                        if((jsonResponse.length()==1)){
                            Toast.makeText(getApplicationContext(),"TO DO updated successfully",Toast.LENGTH_LONG).show();

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("err", error.toString());
                }
            });

            requestQueuereg.add(requestreg);
        }
    }
    /*
           function save to do !!!!!!->>>>> we update the API thank you !!
     */
    public void save(String username, String id,String password,String todo){
        if(checkConnection()) {
            RequestQueue requestQueuereg = Volley.newRequestQueue(this);
            StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=save&username=" + username +"&id="+id+ "&password=" + password+"&todo="+todo, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject jsonResponse = null;
                    try {
                        jsonResponse = new JSONObject(response);
                        if((jsonResponse.length()==1)){
                            Toast.makeText(getApplicationContext(),"TO DO updated successfully",Toast.LENGTH_LONG).show();

                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("err", error.toString());
                }
            });

            requestQueuereg.add(requestreg);
        }
    }
    /*
    function alert : show input to share to do list
     */
    public void alert(final String username, final String password){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Share With Other Username");

// Set up the input
        final EditText input = new EditText(this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
                share(m_Text,username,password,todoedittext.getText().toString());

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
    /*
    function share todo list
     */
    public void share(final String shareusername, final String username, final String password,String todo){
        if(checkConnection()) {
            RequestQueue requestQueuereg = Volley.newRequestQueue(this);
            StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=share&username=" + username + "&password=" + password+"&todo="+todo+"&user="+shareusername, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject jsonResponse = null;
                    try {
                        jsonResponse = new JSONObject(response);
                        if(jsonResponse.length()<2){
                            Toast.makeText(getApplicationContext(),"List Shared With  "+shareusername,Toast.LENGTH_LONG).show();

                        }else if(jsonResponse.length()==2)
                        {
                            Toast.makeText(getApplicationContext(),"Cannot Share With  "+shareusername+" "+jsonResponse.getString("message"),Toast.LENGTH_LONG).show();

                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Cannot Share With  "+shareusername,Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("err", error.toString());
                }
            });

            requestQueuereg.add(requestreg);
        }
    }
    /*
        max of 49 to do list
     */
     public int addlistto(){
         Random rand = new Random();

         int n = 0;

         while(!listid.contains(n)&&listid.size()<49){
             n = rand.nextInt(49) + 1;
             return n;
         }

         return n;
     }
}
