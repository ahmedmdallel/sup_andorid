package com.example.mdallelahmed.sup;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity2 extends AppCompatActivity {
    SQLiteOpenHelper openHelper;
    SQLiteDatabase db;
    Button btnlogin,btnreg;
    EditText editTextusername,editTextpassword;
    Cursor cursor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        openHelper = new DatabaseHelper(this);
        db = openHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM users";
        /*
        If no user is stored in the SQLITE database
         */
        Cursor c = db.rawQuery(selectQuery, null);
        if(!(c.getCount()>0)){
            Intent intent =  new Intent(LoginActivity2.this,MainActivity.class);
            startActivity(intent);
        }
        /*
         End;  If no user is stored in the SQLITE database
         */

        btnlogin = (Button) findViewById(R.id.buttonlogin);
        btnreg = (Button) findViewById(R.id.buttonregister);
        editTextusername = (EditText) findViewById(R.id.editTextusernamelogin);
        editTextpassword = (EditText) findViewById(R.id.editTextpasswordlogin);
        synDb();
        /*
        login button function
         */
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = editTextusername.getText().toString();
                String password = editTextpassword.getText().toString();
                cursor = db.rawQuery("SELECT * FROM "+DatabaseHelper.TABLE_NAME+" WHERE "+DatabaseHelper.COL_2+"=? AND "+DatabaseHelper.COL_3+"=?",new String[]{username,password});
                if(cursor != null){
                    if(cursor.getCount()>0){
                        Toast.makeText(getApplicationContext(),"login successfully",Toast.LENGTH_LONG).show();
                        Intent intent =  new Intent(LoginActivity2.this,ToDoListActivity.class);
                        intent.putExtra("username",editTextusername.getText().toString());
                        intent.putExtra("password",editTextpassword.getText().toString());
                        startActivity(intent);

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"login error",Toast.LENGTH_LONG).show();

                    }
                }
            }
        });
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(LoginActivity2.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
    public void synDb(){
        db = openHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM users";
        Cursor c = db.rawQuery(selectQuery, null);

        while (c.moveToNext()) {

            final String id,username,password,fristname,lastname,email;
            username = c.getString(c.getColumnIndex("username"));
            password = c.getString(c.getColumnIndex("password"));
            fristname = c.getString(c.getColumnIndex("firstname"));
            lastname = c.getString(c.getColumnIndex("lastname"));
            email = c.getString(c.getColumnIndex("email"));

            if(checkConnection()) {
                RequestQueue requestQueuereg = Volley.newRequestQueue(this);
                StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=login&username=" + username + "&password=" + password, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonResponse = null;
                        try {
                            jsonResponse = new JSONObject(response);
                            if(jsonResponse.length()==2){
                                addUser(username,password,fristname,lastname,email);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("err", error.toString());
                    }
                });

                requestQueuereg.add(requestreg);
            }
        }
        c.close();

    }
    /*
///function to check internet connection
*/
    public boolean checkConnection(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        else
            return false;
    }
    /*
    function add user in my sql data base using API
     */
    public void addUser(String username,String password,String fname,String lname,String email){
        if(checkConnection()) {
            RequestQueue requestQueuereg = Volley.newRequestQueue(this);
            StringRequest requestreg = new StringRequest(Request.Method.GET, "http://10.0.2.2:8888/index.php?action=register&username=" + username + "&password=" + password + "&firstname=" + fname + "&lastname=" + lname + "&email=" + email, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("succ", response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("err", error.toString());

                }
            });

            requestQueuereg.add(requestreg);
        }
    }

}
