<?php
@session_start();

abstract class DDBHelper {
    
    
    private static $PDOInstance = null;
  
    private static $error = false;
  
    //private static $instance = null;

    const DEFAULT_SQL_TYPE = "mysql";
  
    const DEFAULT_SQL_USER = "root";

    const DEFAULT_SQL_HOST = "localhost";

    const DEFAULT_SQL_PASS = "root";

    const DEFAULT_SQL_DTB = "sup";


    
    public static function getInstance() {
        if(is_null(self::$PDOInstance)) {
            try{
                self::$PDOInstance = new PDO(self::DEFAULT_SQL_TYPE.':dbname='.self::DEFAULT_SQL_DTB.';host='.self::DEFAULT_SQL_HOST,self::DEFAULT_SQL_USER ,self::DEFAULT_SQL_PASS,
                        array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));
            }
            catch (Exception $e) {
                self::$error = true;
                die('Erreur : ' . $e->getMessage());
            }
        }
        return self::$PDOInstance;
    }
  
    public static function isInError() {  
        return self::$error;
    }
    
    
    
    
    
    
    
    public function deleteById($table, $id) {
        
        if(!empty($id) && !empty($table)) {
            $query   = "DELETE FROM ".$table." WHERE id = :id";
            $request = self::getInstance()->prepare($query);
            $request->bindValue(':id', $id);
            
            if ($request->execute()) {
        return TRUE;
        }
        }
       
    return FALSE;
    }
    
    public static function prepare($query) {
        return self::getInstance()->prepare($query);
    }
    
    public static function query($query) {
        return self::getInstance()->query($query);
    }

    public static function exec($query, $params = null, $fetch = false) {
        $request = self::getInstance()->prepare($query);
        if(is_array($params)) {
            foreach ($params as $key => $value) {
                $request->bindValue(':'.$key, $value);
            }
        }
        $request->execute();
        $result = null;
        if($fetch) {
            $result = $request->fetchAll();
        }
        return $result;
    }
  
    public static function execAssoc($query, $params = null) {
        $request = self::getInstance()->prepare($query);
        if(is_array($params)) {
            foreach ($params as $key => $value) {
                $request->bindValue(':'.$key, $value);
            }
        }
        $request->execute();
        $result = $request>fetchAll(PDO::FETCH_ASSOC);
        return $result;
  }
    
    public static function execute($query, $objectType, $params = NULL) {
        if (!empty($query) && is_string($query) && !empty($objectType) && is_string($objectType)) {
            $pdo   = self::getInstance();
            $result = null;
            
            try {
                $request = $pdo->prepare($query);
                
                if (!empty($params) && is_array($params)) {
                    foreach ($params as $key => $value) {
                        $request->bindValue(':'.$key, $value);
                    }      
                }
                
                $request->execute();
                $result = $request->fetchAll(PDO::FETCH_CLASS, $objectType);
            } catch(PDOException $ex) {
                self::$error = true;
            }
            return $result;
        }
        else {
            return false;
        }
    }
    
    public static function load($table, $objectType, $start = 0, $end = null) {
        
        if (!empty($table) && is_string($table) && !empty($objectType) && is_string($objectType)) {
            $pdo   = self::getInstance();
            $result = null;
            $query = 'SELECT * FROM '.$table;
            
            try {
                $request = $pdo->prepare($query);
                
                if(is_numeric($end) && $end > 0  && is_numeric($start)) {
                    $query .= ' LIMIT '.$start.', '.$end;
                }
                
                $request->execute();
                $result = $request->fetchAll(PDO::FETCH_CLASS, $objectType);
            } catch(PDOException $ex) {
                self::$error = true;
            }
            
            return $result;
        }
        else {
            return false;
        }
    }
    
    public static function justOne($array){
        if(is_array($array) && isset($array[0])){
            return $array[0];
        }
        else {
            return $array;           
        }
    }
    

    
    
    
    public static function beginTransaction() {
        self::getInstance()->beginTransaction();
    }

    public static function commit() {
        if(self::getInstance()->inTransaction()) {
            self::getInstance()->commit();
        }
    }

    public static function rollBack() {
        if(self::getInstance()->inTransaction()) {
            self::getInstance()->rollBack();
        }
    }

    public static function inTransaction() {
        return self::getInstance()->inTransaction();
    }
    
    public static function getLastInsertId() {
        return self::getInstance()->lastInsertId();
    }
    
    
    
    
    
    public static function truncate($table){
        if (!empty($table)) {
        $query   = "TRUNCATE TABLE ".$table." ;";
        $request = self::getInstance()->prepare($query);
        return $request->execute();
    }
    return FALSE;
    }
    
    public static function resetAI($table){
        $query = "ALTER TABLE ".$table." AUTO_INCREMENT = 1";
        $request = self::getInstance()->prepare($query);
        return $request->execute();
    }
    
   
}


class User {
    public $id;
    public $username;
    public $password;
    public $firstname;
    public $lastname;
    public $email;
    
    public function __construct() {
    
    }
}

class TodoList {
    public $id;
    public $lastupdate;
    public $usercreator;
    public $userinvited;
    public $todo;

    public function __construct() {
    
    }
}

class UserDao extends DDBHelper {

    public static $TABLE = "users";
  
  
  
  public static function save(User $user){
      $query = "INSERT INTO ".UserDao::$TABLE." (`id`, `username`, `firstname`, `lastname`, `email`, `password`) "
              . "VALUES (NULL, :username, :firstname, :lastname, :email, :password);";
        $params = array(
            'username' => $user->username, 
            'firstname' => $user->firstname, 
            'lastname' => $user->lastname, 
            'email' => $user->email, 
            'password' => $user->password
        );
        self::beginTransaction();
        try{
            
            self::exec($query, $params);
            $user->id = self::getLastInsertId();
            self::commit();
            if($user->id <= 0) {
                return false;
            }
        }
        catch (PDOException $ex) {
            self::rollBack();
            return false;
        }
        return $user;
  }
  
  public static function getById($id){
      $query = "SELECT * FROM ".UserDao::$TABLE." WHERE id = :id";
      return self::justOne(self::execute($query, "User", array('id' => $id)));
  }
  
  public static function getByUserName($userName) {
      $query = "SELECT * FROM ".UserDao::$TABLE." WHERE username = :username";
      return self::justOne(self::execute($query, "User", array('username' => $userName)));
  }
  
  public static function getByUserNameAndPassword($userName, $password) {
      $query = "SELECT * FROM ".UserDao::$TABLE." WHERE username = :username AND password = :password";
      return self::justOne(self::execute($query, "User", array(
          'username' => $userName,
          'password' => $password
       )));
  }
}


class TodoListDao extends DDBHelper {

    public static $TABLE = "todolists";
  
  
  public static function save(TodoList $todolist){

      $query = "INSERT INTO ".TodoListDao::$TABLE." (`id`, `lastupdate`, `usercreator`, `userinvited`, `todo`) "
              . "VALUES (:id, CURRENT_TIMESTAMP, :usercreator, :userinvited, :todo) "
              ."ON DUPLICATE KEY UPDATE lastupdate= CURRENT_TIMESTAMP, userinvited = :userinvited, todo= :todo;";
        $params = array(
            'id' => $todolist->id,  
            'usercreator' => $todolist->usercreator,
            'userinvited' => $todolist->userinvited, 
            'todo' => $todolist->todo

        );
        self::beginTransaction();
        try{
            
            self::exec($query, $params);
            
            $todolist->id = self::getLastInsertId();

             var_dump($todolist);
            self::commit();
            if($todolist->id <= 0) {return false;}
        }
        catch (PDOException $ex) {
            self::rollBack();
            return false;
        }
        return $todolist;
  }
  
  
  public static function getById(User $user, $id){
      $query = "SELECT * FROM ".TodoListDao::$TABLE." WHERE id = :id AND (usercreator = :usercreator OR userinvited = :userinvited) ORDER BY id ASC;";
      return self::justOne(self::execute($query, "TodoList", array(
        'id' => $id,
        'usercreator' => $user->id,
        'userinvited' => $user->id
        )));
  }

public static function listByUser(User $user) {
    
      $query = "SELECT * FROM ".TodoListDao::$TABLE." WHERE usercreator = :usercreator OR userinvited = :userinvited ORDER BY id ASC;";
    
      return self::execute($query, "TodoList", array(
        'usercreator' => $user->id,
        'userinvited' => $user->id
        ));
  }
  
public static function listShared(User $user,User $friend) {
    
      $query = "SELECT * FROM ".TodoListDao::$TABLE." WHERE (usercreator = :usercreator AND userinvited = :userinvited) OR (usercreator = :userinvited AND userinvited = :usercreator) LIMIT 1;";
    
      return self::justOne(self::execute($query, "TodoList", array(
        'usercreator' => $user->id,
        'userinvited' => $friend->id
        )));
  }
  

  

  
  


}




















if(isset($_POST["action"]) || isset($_GET["action"])){
    $result["success"] = false;

    if($_REQUEST["action"] == "register") {
        if($_REQUEST["username"] && $_REQUEST["password"] && $_REQUEST["firstname"] && $_REQUEST["lastname"] && $_REQUEST["email"]){
            $user = new User();
                $user->username = $_REQUEST["username"];
                $user->password = $_REQUEST["password"];
                $user->firstname = $_REQUEST["firstname"];
                $user->lastname = $_REQUEST["lastname"];
                $user->email = $_REQUEST["email"];

                if(UserDao::getByUserName($_REQUEST["username"])){
                    $result["message"] = "this username already exist";
                }
                else {
                    $user = UserDao::save($user);
                    if($user) {
                        $_SESSION['user'] = $user;
                        echo json_encode($user);
                        die();
                    }
                }
        }
        else {
            $result["success"] = false;
        }


    }
    elseif($_REQUEST["action"] == "login") {
        if($_REQUEST["username"] && $_REQUEST["password"]){
        
                $user = UserDao::getByUserNameAndPassword($_REQUEST["username"], $_REQUEST["password"]);
                if($user) {
                    $_SESSION['user'] = $user;
                    echo json_encode($user);
                    die();
                }
                else {
                    $result["success"] = false;
                    $result["message"] = "not found";
                }
        }
        else {
            $result["success"] = false;
        }
    }
    elseif($_REQUEST["action"] == "logout") {
        unset($_SESSION['user']);
        $result["success"] = true;
    }
    elseif($_REQUEST["action"] == "list") {
        if($_REQUEST["username"] && $_REQUEST["password"]){
                $user = UserDao::getByUserNameAndPassword($_REQUEST["username"], $_REQUEST["password"]);
                if($user) {
                    $_SESSION['user'] = $user;
                }
        }
        
        if($_SESSION['user'] && $_SESSION['user']->id > 0) {
            //list all creator + invited
            $todolists = array();

            $todolists = TodoListDao::listByUser($_SESSION['user']);
            if(count($todolists) ==0) { //my bad, I save a new todo
                $todo = new TodoList();
                $todo->usercreator = $_SESSION['user']->id;
                $todo->todo = '';
                TodoListDao::save($todo);
                $todolists = TodoListDao::listByUser($_SESSION['user']);
            }

            echo json_encode($todolists);die();
        }
        else {
            $result["success"] = false;
        }
    }
    elseif($_REQUEST["action"] == "read") {
        $id = 0;
        if($_REQUEST["username"] && $_REQUEST["password"]){
            
                $user = UserDao::getByUserNameAndPassword($_REQUEST["username"], $_REQUEST["password"]);
                
                if($user) {
                    $_SESSION['user'] = $user;
                }
        }
        if($_SESSION['user'] && $_SESSION['user']->id > 0) {
            //list all creator + invited
            if($_REQUEST["id"]) {$id = $_REQUEST["id"];}
            $todolist = TodoListDao::getById($_SESSION['user'], $id);
        
            if($todolist->id >0) { 
                echo json_encode($todolist);die();
            }
            else {
                $result["success"] = false;
                $result["message"] = "not found";
            }
        }
        else {
            $result["success"] = false;
        }
    }
    elseif($_REQUEST["action"] == "update") {
        $id = 0;
        if($_REQUEST["username"] && $_REQUEST["password"]){
                $user = UserDao::getByUserNameAndPassword($_REQUEST["username"], $_REQUEST["password"]);
                if($user) {
                    $_SESSION['user'] = $user;
                }
        }
        if($_SESSION['user'] && $_SESSION['user']->id > 0) {
            //list all creator + invited
            if($_REQUEST["id"]) {$id = $_REQUEST["id"];}
            $todolist = TodoListDao::getById($_SESSION['user'], $id);
            
            if($todolist->id >0) { 
                //update
                if($_REQUEST["todo"] && !empty(trim($_REQUEST["todo"]))) {

                    $todolist->todo = $_REQUEST["todo"];
                    TodoListDao::save($todolist);
                    $result["success"] = true;
                }
            }
            else {
                $result["success"] = false;
                $result["message"] = "not found";
            }
        }
        else {
            $result["success"] = false;
        }

    }
    elseif($_REQUEST["action"] == "share") {
        if($_REQUEST["username"] && $_REQUEST["password"]){
                $user = UserDao::getByUserNameAndPassword($_REQUEST["username"], $_REQUEST["password"]);
                if($user) {
                    $_SESSION['user'] = $user;
                }
        }
        
        if($_SESSION['user'] && $_SESSION['user']->id > 0) {
            
            //check username
            if($_REQUEST["user"]) {
                $friend = UserDao::getByUserName($_REQUEST["user"]);
                if($friend && $friend->id >0) {
                    $todolist = TodoListDao::listShared($_SESSION['user'], $friend);
                    
                    if(is_array($todolist) && count($todolist) ==0) {

                        $todo = new TodoList();
                        $todo->usercreator = $_SESSION['user']->id;
                        $todo->userinvited = $friend->id;
                        $todo->todo = $_REQUEST["todo"];
                        var_dump($todo);
                        TodoListDao::save($todo);
                        $result["success"] = true;
                    }
                    else {
                        $result["success"] = false;
                        $result["message"] = "already exist id:".$todolist->id;
                    }
                }
                else {
                    $result["success"] = false;
                    $result["message"] = "Friend not found";
                }
            }

        }
        else {
            $result["success"] = false;
        }
    }
    elseif($_REQUEST["action"] == "save") {
        if($_REQUEST["username"] && $_REQUEST["password"]){
                $user = UserDao::getByUserNameAndPassword($_REQUEST["username"], $_REQUEST["password"]);
                if($user) {
                    $_SESSION['user'] = $user;
                }
        }
        
        if($_SESSION['user'] && $_SESSION['user']->id > 0) {
            

                        $todo = new TodoList();
                        $todo->usercreator = $_SESSION['user']->id;
                        $todo->userinvited = $_REQUEST["user"];
                        $todo->todo = $_REQUEST["todo"];
                        var_dump($todo);
                        TodoListDao::save($todo);
                        $result["success"] = true;
                
                }
             else {
            $result["success"] = false;
        }
    }
      
  

    
    
    
    
    echo json_encode($result);
    die();
}


?>































<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Welcome!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   
    <meta name="robots" content="noindex" />
	<style type="text/css"><!--
    body {
        color: #444444;
        background-color: #EEEEEE;
        font-family: 'Trebuchet MS', sans-serif;
        font-size: 80%;
    }
    h1 {}
    h2 { font-size: 1.2em; }
    #page{
        background-color: #FFFFFF;
        width: 60%;
        margin: 24px auto;
        padding: 12px;
    }
    #header{
        padding: 6px ;
        text-align: center;
    }
    .header{ background-color: #83A342; color: #FFFFFF; }
    #content {
        padding: 4px 0 24px 0;
    }
    #footer {
        color: #666666;
        background: #f9f9f9;
        padding: 10px 20px;
        border-top: 5px #efefef solid;
        font-size: 0.8em;
        text-align: center;
    }
    #footer a {
        color: #999999;
    }
    --></style>
</head>
<body>
    
    <div id="page">
        <div id="header" class="header">
            <h1>Welcome to SUPTodo API</h1>
        </div>
        <div id="content">
            <h2>This is the API of SUPTodo.</h2>
            
            <p>For questions or problems please contact steve.colinet@supinfo.com .</p>
            <p></p><p></p>
            <p>This API works by HTTP POST or GET.</p>
            <p>To test, you can use create an account.</p>
            <p>You can use http session or just send username and password for each method</p>
            <p><h4>Availables methods:</h4></p>
            <p></p>


















<p><h4>- To register:</h4></p>
<p>Parameters:</p>
<p>- action=register</p>
        <p>- username= “an username”</p>
        <p>- password= “the password”</p>
        <p>- firstname= “firstname”</p>
        <p>- lastname= “lastname”</p>
        <p>- email= “email”</p>

    <p></p>
<p>If successful, returns a json of the current user.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
<p>{"id":42,"username":"steve","password":"******","firstname":"steve","lastname":"colinet","email":"62572@supinfo.com"}</p> 
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>







<p><h4>- To login:</h4></p>
<p>Parameters:</p>
<p>- action=login</p>
        <p>- username= “an username”</p>
        <p>- password= “the password”</p>
    <p></p>
<p>If successful, returns a json of the current user.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
<p>{"id":42,"username":"steve","password":"******","firstname":"steve","lastname":"colinet","email":"62572@supinfo.com"}</p> 
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>






<p><h4>- To logout:</h4></p>
<p>Parameters:</p>
<p>- action=logout</p>
        <p>- username= “an username” (optional)</p>
        <p>- password= “the password” (optional)</p>
    <p></p>
<p>If successful, returns a state "success" as "true" in a JSON object.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
<p>{"success":true}</p> 
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>

























<p><h4>- To list todolist:</h4></p>
<p>Parameters:</p>
<p>- action=list</p>
		<p>- username= “an username” (optional)</p>
		<p>- password= “the password” (optional)</p>
	<p></p>
<p>If successful, returns a json of todolists.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
<p>[{"id":"2","lastupdate":"2018-12-11 10:50:16","usercreator":"42","userinvited":null,"todo":"ranger bureau, ménage"},{"id":"4","lastupdate":"2018-12-11 10:31:33","usercreator":"42","userinvited":43,"todo":"soda, chips"}]</p> 
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>























<p><h4>- To read a todolist:</h4></p>
<p>Parameters:</p>
<p>- action=read</p>
		<p>- username= “an username” (optional)</p>
		<p>- password= “the password” (optional)</p>
		<p>- id= “a todo id”</p>
	<p></p>
<p>If successful, returns a json of todolist.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
{"id":"2","lastupdate":"2018-12-11 10:50:16","usercreator":"42","userinvited":null,"todo":"ranger bureau, ménage"}
<p>
</p> 
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>





























<p><h4>- To update the todolist:</h4></p>
<p>Parameters:</p>
<p>- action=update</p>
		<p>- username= “an username” (optional)</p>
		<p>- password= “the password” (optional)</p>
		<p>- id= “a todo id”</p>
		<p>- todo= “the text”</p>
	<p></p>
<p>If successful, returns a state "success" as "true" in a JSON object.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
<p>{"success":true}<br /></p>
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>





















<p><h4>- To create a shared todolist:</h4></p>
<p>Parameters:</p>
<p>- action=share</p>
        <p>- username= “an username” (optional)</p>
        <p>- password= “the password” (optional)</p>
        <p>- user= “the username of the friend”</p>

    <p></p>
<p>If successful, returns a state "success" as "true" in a JSON object.</p>
<p>If failure, returns a state "success" as "false" in a JSON object.</p>
<p>Success example: </p>
<p>{"success":true}<br /></p>
<p>Fail example : </p>
<p>{"success":false}<br /></p>
<p><br /></p>




















<p><br /><br /></p>
<p>You can use this address or you can download this script of API:</p>
<p><a href="index.php.zip">To download</a></p>
        </div>
        <div id="footer">
            <p>Powered by <a href="http://www.supinfo.com">SUPTodo</a></p>
        </div>
    </div>
</body>
</html>
